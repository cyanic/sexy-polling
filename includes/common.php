<?php

function wpsxp_get_ip() {
    //get ip address
    $REMOTE_ADDR = null;
    if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) { $REMOTE_ADDR = $_SERVER['HTTP_X_FORWARDED_FOR']; }
    elseif(isset($_SERVER['HTTP_CLIENT_IP'])) { $REMOTE_ADDR = $_SERVER['HTTP_CLIENT_IP']; }
    elseif(isset($_SERVER['HTTP_VIA'])) { $REMOTE_ADDR = $_SERVER['HTTP_VIA']; }
    elseif(isset($_SERVER['REMOTE_ADDR'])) { $REMOTE_ADDR = $_SERVER['REMOTE_ADDR']; }
    else { $REMOTE_ADDR = 'Unknown'; }
    return $REMOTE_ADDR;
}

function wpsxp_get_geoip($ip) {
    $api_key = 'YOUR IPInfoDB.com API KEY HERE';
    $url = "http://api.ipinfodb.com/v3/ip-city/?key={$api_key}&format=json&ip={$ip}";
    $ch = curl_init ($url) ;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) ;
    $output = curl_exec($ch) ;
    curl_close($ch) ;
    
    $result = array(
        'country_name' => 'Unknown',
        'city_name' => 'Unknown',
        'region_name' => 'Unknown',
        'country_code' => 'Unknown'
    );
    
    if ($output !== false) {
        $parsed = json_decode($output);
        if ($parsed !== NULL && $parsed->statusCode === 'OK') {
            if ($parsed->countryName !== '' && $parsed->countryName !== '-') $result['country_name'] = mysql_real_escape_string($parsed->countryName);
            if ($parsed->cityName !== '' && $parsed->cityName !== '-') $result['city_name'] = mysql_real_escape_string($parsed->cityName);
            if ($parsed->regionName !== '' && $parsed->regionName !== '-') $result['region_name'] = mysql_real_escape_string($parsed->regionName);
            if ($parsed->countryCode !== '' && $parsed->countryCode !== '-') $result['country_code'] = mysql_real_escape_string($parsed->countryCode);
        }
    }
    
    return $result;
}

function wpsxp_get_poll($id) {
    global $wpdb;
    $prefix = $wpdb->prefix;
    
    return $wpdb->get_row($wpdb->prepare("SELECT * FROM `{$prefix}wpsxp_sexy_polls` WHERE `id` = %d", $id));    
}
